Name:           Frontend
Version:        1.0.0
Release:        2%{?dist}
Summary:        Package for Device Import Frontend.

License:        <<Company Name>>
URL:            <<Company URL>>
Source0:        Frontend-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch 
Requires:	Vendor
Requires:	Exceltocsv

%description
Package for Device Import Frontend.
The package will need to be installed on Zabbix Web Server.

%define frontendir  /var/www/html/automation_tools
%define confdir %{_sysconfdir}/httpd/conf.d/

%prep
%setup -q -n %{name}-%{version}

%install
rm -rf %{buildroot}
%{__install} -d %{buildroot}%{frontendir}
%{__cp} -Rf application %{buildroot}%{frontendir}
%{__cp} -Rf system %{buildroot}%{frontendir}
%{__cp} -Rf assets %{buildroot}%{frontendir}
%{__cp} -Rf CAS %{buildroot}%{frontendir}
%{__cp} -Rf CAS.php %{buildroot}%{frontendir}
%{__cp} -Rf composer.json %{buildroot}%{frontendir}
%{__cp} -Rf contributing.md %{buildroot}%{frontendir}
%{__cp} -Rf index.php %{buildroot}%{frontendir}
%{__cp} -Rf license.txt %{buildroot}%{frontendir}
%{__cp} -Rf readme.rst %{buildroot}%{frontendir}
%{__cp} -Rf automation_tools.conf %{buildroot}%{frontendir}
#%{__cp} -Rf .htaccess %{buildroot}%{frontendir}
%{__cp} -Rf rewrite %{buildroot}%{frontendir}

%pre
mkdir -p %{frontendir}
mkdir -p %{frontendir}/assets/uploads
touch %{frontendir}/assets/uploads/SGTS_NA_CMDB_IMPORT_REMOVE_SPACES2T.csv

%post
chmod -R 777 %{frontendir}/assets/
chmod -R 777 %{frontendir}/assets/uploads
mv %{frontendir}/rewrite %{frontendir}/.htaccess
mv %{frontendir}/automation_tools.conf %{confdir}
service httpd restart

%files
%defattr(-,root,root,-)
%{frontendir}/*
%doc



%changelog
* Tue Apr 10 2018 sadhana shinde<Email ID> - 1.0.0-2
- frontend

