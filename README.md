# Steps to create RPM Package - 

References - 
	[https://rpm-packaging-guide.github.io/](https://rpm-packaging-guide.github.io/)

###  1.Create tree structure 

```bash
$ tree ~/rpmbuild/
```
```html
	|-- BUILD
	|-- RPMS
	|-- BUILDROOT
	|-- SOURCES
	|-- SPECS
	|-- SRPMS
```

###   2.Consider you have frontend project named automation_tools

```html
	|--automation_tools
	   |--application
	   |--assets
	   |--CAS
	   |--CAS.php
	   |--composer.json
	   |--contributing.md
	   |--index.php
	   |--license.txt
	   |--readme.rst
	   |--system
	   |--.htaccess
```

-     create a directory named Frontend-1.0.0.
-     copy all the files from  automation_tools to  Frontend-1.0.0 directory.
-     For frontend to work you will need .conf file in /etc/httpd/conf.d/ path.
-     create automation_tools.conf in  Frontend-1.0.0 and put below code in it.
    
```html
			<Directory /var/www/html/automation_tools>
					Options +Indexes
					Order Deny,Allow
					Allow from All
					AllowOverride All
					Require all granted
					<IfModule mod_php5.c>
							php_value date.timezone Asia/Kolkata
				   </IfModule>
			</Directory>

			Alias /automation_tools /var/www/html/automation_tools
```
-     Also rename .htaccess file as rewrite.
    
###   3.Create .tar.gz of Frontend-1.0.0 

```bash
	$ tar -zcvf Frontend-1.0.0.tar.gz Frontend-1.0.0
```
	Now you will have directory structre mentioned as below.
	|--automation_tools
	|--Frontend-1.0.0
	|--Frontend-1.0.0.tar.gz
	a.Copy Frontend-1.0.0.tar.gz to ~/rpmbuild/SOURCES/

```bash
	$ cp Frontend-1.0.0.tar.gz ~/rpmbuild/SOURCES/
```

###   4.Create Spec file named Frontend.spec.
	Spec file is attached for reference.
	copy the spec file to /rpmbuild/SPECS/ directory.

```bash
	$ cp Frontend.spec ~/rpmbuild/SPECS/
```

###   5.Go to ~/rpmbuild/SPECS/ and execute below mentioned commands.

```bash
	rpmdev-bumpspec --comment="<<comment>>" --userstring="username <Email ID>" Frontend.spec
	rpmbuild -ba Frontend.spec
	sudo cp ~/rpmbuild/RPMS/noarch/Frontend-1.0.0-2.noarch.rpm /var/www/html/sgts-rpms-repo/
	sudo createrepo --update /var/www/html/sgts-rpms-repo/
```

###   6.Login to the server where you want to install this package.

```bash
	$ sudo yum clean metadata
	$ sudo yum list | grep -i frontend
```

	Frontend-1.0.0-2.noarch
	If you want to check what all packages are installed on server 

```bash
	rpm -qa | grep -i sgts
```